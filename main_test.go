package main

import (
	"testing"
)

func Test_main(t *testing.T) {
	main()
}

func Test_createConfig(t *testing.T) {
	CreateConfig(
		"git-gateway",
		true,
		"editorial_workflow",
		"/static/assets",
		"/static/assets",
		"uploadcare",
		nil,
		"localhost",
		"localhost",
		"https://casino.pamestoixima.gr/assets/static/images/mobileLogo.png",
		"en",
		false,
		&SlugConfig{
			Encoding:            "unicode",
			CleanAccents:        true,
			SanitizeReplacement: "_",
		},
	)
}
